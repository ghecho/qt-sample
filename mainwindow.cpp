#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->lineEdit->setStyleSheet("background-color:black; color:white;");

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(makeAction()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::makeAction()
{
    ui->label->setText("text is: " + ui->lineEdit->text());
}
